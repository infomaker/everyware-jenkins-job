#!/bin/bash

STARTJOBTIME=$(date +%s)
CACHE_BUST=$(date +%s%3N)
WORKINGDIR="$(pwd)"

# Configured Variables
# ==============================
PROJECT="customer"
ENVIRONMENT="stage"
PRODUCTION=false
AWS_CUSTOMER_REGION="REPLACE_CUSTOMER_REGION"

# S3 Variables
BUCKET="REPLACE_DEPLOY_BUCKET"
BUCKET_STATIC="REPLACE_STATIC_BUCKET"

# New Relic Variables
NEW_RELIC_APP_ID="${PROJECT}$(${PRODUCTION} && echo "prod" || echo "stage")"
NEW_RELIC_API_KEY=""

# Cloudfront Variables
CLOUDFRONT_ID=""

# Generated Variables
# ==============================
ENVIRONMENTNAME="${PROJECT}-${ENVIRONMENT}"
APPLICATIONNAME="${PROJECT}-application"
VERSIONLABEL="${APPLICATIONNAME}-${GIT_COMMIT}-${ENVIRONMENTNAME}-$(date +"%Y%m%d-%H%M")"
FILENAME="${VERSIONLABEL}.zip"

# CODE FIXES
# ==============================
# Inject changelog to text-file.
echo -e "$CHANGELOG" >www/wp-config/changelog.txt

# Inject GIT_COMMIT to wp-config.php
echo -e "define('GIT_COMMIT', '${CACHE_BUST}');" >>www/wp-config/wp-config-aws.php

# Change region in docker-compose.yml
 sed -i "s|AWS_REGION|$AWS_CUSTOMER_REGION|g" docker-compose.yml

if ${PRODUCTION}; then
  # Replacing container memory settings for prod env.
  sed -i "s|PHP_MEM|2048m|g" docker-compose.yml
  sed -i "s|NGX_MEM|512m|g" docker-compose.yml
else
  # Replacing container memory settings for stage/test env.
  sed -i "s|PHP_MEM|512m|g" docker-compose.yml
  sed -i "s|NGX_MEM|256m|g" docker-compose.yml
fi

# Initiate and build application
# ==============================

# Clean builds directory
rm -rf builds

/bin/bash bin/init.sh $(${PRODUCTION} && echo "--production" || echo "--stage")
RETURNCODE=$?
if [[ $RETURNCODE != 0 ]]; then
  echo "Failed to initiate application."
  exit 1
fi

# Set up proper robots.txt for stage
if [ "${PRODUCTION}" = false ] ; then
  rm www/robots.txt
  cat <<-EOF > www/robots.txt
  User-agent: *
  Disallow: /
  EOF
fi

# Fix permission issue when files and folders are owned by root
docker run --rm -v ${WORKINGDIR}:/app busybox:latest chmod -R 777 /app

# Create builds directory
mkdir builds

# Build the zip with exclude-file
zip --symlinks -r builds/${FILENAME} . -x@.exclude-from-zip
RETURNCODE=$?
if [[ $RETURNCODE -ne "0" ]]; then
  echo "Failed to create application zip"
  exit 1
fi

# Upload to Amazon S3
# ==============================
aws s3 cp builds/${FILENAME} s3://${BUCKET}/
RETURNCODE=$?
if [[ $RETURNCODE != 0 ]]; then
  echo "Upload of \"builds/${FILENAME}\" to Amazon S3 exited with errorcode ${RETURNCODE}."
  exit 1
fi

# Add application-version to Elastic Beanstalk
# ==============================
aws elasticbeanstalk create-application-version \
  --application-name "${APPLICATIONNAME}" \
  --version-label "${VERSIONLABEL}" \
  --source-bundle "S3Bucket=${BUCKET},S3Key=${FILENAME}"
RETURNCODE=$?
if [[ $RETURNCODE != 0 ]]; then
  echo "Creation of: \"${APPLICATIONNAME}\" in Elastic Beanstalk exited with errorcode ${RETURNCODE}."
  exit 1
fi

########
# Upload files to a separate S3 bucket for CloudFront.
# Should only be enabled in the production Jenkins job.
########

if ${PRODUCTION}; then

  # CSS
  aws s3 cp www/wp-content s3://${BUCKET_STATIC}/wp-content --recursive --exclude "*" \
    --include "plugins/*.css" --include "themes/*.css" --include "mu-plugins/*.css" \
    --exclude "*/.*" --exclude "*/compile/*" --exclude "*/node_modules/*" --exclude "*/twig/*" --exclude "*/less/*" --cache-control max-age=31536000 --storage-class REDUCED_REDUNDANCY
  RETURNCODE=$?
  if [[ $RETURNCODE != 0 ]]; then
    echo "Upload of CSS files to S3 exited with errorcode ${RETURNCODE}."
    exit 1
  fi

  # JS
  aws s3 cp www/wp-content s3://${BUCKET_STATIC}/wp-content --recursive --exclude "*" \
    --include "plugins/*.js" --include "themes/*.js" --include "mu-plugins/*.js" \
    --exclude "*/.*" --exclude "*/compile/*" --exclude "*/node_modules/*" --exclude "*/twig/*" --exclude "*/less/*" --cache-control max-age=31536000 --storage-class REDUCED_REDUNDANCY
  RETURNCODE=$?
  if [[ $RETURNCODE != 0 ]]; then
    echo "Upload of JS files to S3 exited with errorcode ${RETURNCODE}."
    exit 1
  fi

  # FONTS
  aws s3 cp www/wp-content s3://${BUCKET_STATIC}/wp-content --recursive --exclude "*" \
    --include "plugins/*.eot*" --include "plugins/*.woff2" --include "plugins/*.woff" --include "plugins/*.ttf" --include "plugins/*.svg#*" \
    --include "themes/*.eot*" --include "themes/*.woff2" --include "themes/*.woff" --include "themes/*.ttf" --include "themes/*.svg#*" \
    --include "mu-plugins/*.eot*" --include "mu-plugins/*.woff2" --include "mu-plugins/*.woff" --include "mu-plugins/*.ttf" --include "mu-plugins/*.svg#*" \
    --exclude "*/.*" --exclude "*/compile/*" --exclude "*/node_modules/*" --exclude "*/twig/*" --exclude "*/less/*" --cache-control max-age=31536000 --storage-class REDUCED_REDUNDANCY
  RETURNCODE=$?
  if [[ $RETURNCODE != 0 ]]; then
    echo "Upload of Fonts to S3 exited with errorcode ${RETURNCODE}."
    exit 1
  fi

  # IMAGES
  aws s3 cp www/wp-content s3://${BUCKET_STATIC}/wp-content --recursive --exclude "*" \
    --include "plugins/*.jpg" --include "plugins/*.jpeg" --include "plugins/*.gif" --include "plugins/*.png" --include "plugins/*.bmp" --include "plugins/*.svg" \
    --include "themes/*.jpg" --include "themes/*.jpeg" --include "themes/*.gif" --include "themes/*.png" --include "themes/*.bmp" --include "themes/*.svg" --include "themes/*.ico" \
    --include "mu-plugins/*.jpg" --include "mu-plugins/*.jpeg" --include "mu-plugins/*.gif" --include "mu-plugins/*.png" --include "mu-plugins/*.bmp" --include "mu-plugins/*.svg" --include "mu-plugins/*.ico" \
    --exclude "*/.*" --exclude "*/compile/*" --exclude "*/node_modules/*" --exclude "*/twig/*" --exclude "*/less/*" --cache-control max-age=86400 --storage-class REDUCED_REDUNDANCY
  RETURNCODE=$?
  if [[ $RETURNCODE != 0 ]]; then
    echo "Upload of Images to S3 exited with errorcode ${RETURNCODE}."
    exit 1
  fi
fi

# Function to check status of environment.
# Deployment to AWS will start once environment is available.
function WaitEBAvailable() {
  local EB_NAME="${1}"
  COUNT=1
  SLEEP=10

  until [[ $(aws elasticbeanstalk describe-environments --environment-names ${EB_NAME} | jq -r .Environments[].Status) == "Ready" ]]; do
    if [[ ${COUNT} -eq 120 ]]; then
      echo "[ERROR] (WaitEBAvailable) Will not wait for ${EB_NAME} to become ready any longer ($((${COUNT} * ${SLEEP})) seconds)!"
      exit 1
    fi
    sleep ${SLEEP}
    ((COUNT++))
  done
}

# Put the timestamp into a variable before the deploy begin.
AWS_TIMESTAMP_NOW=$(date '+%Y-%m-%dT%T.%3Z')

# Wait until Beanstalk environment has status Ready.
WaitEBAvailable "${ENVIRONMENTNAME}"

# Deploying the application to environment.
aws elasticbeanstalk update-environment \
  --environment-name "${ENVIRONMENTNAME}" \
  --version-label "${VERSIONLABEL}" \
  --debug

# Wait until Beanstalk environment has status Ready.
WaitEBAvailable "${ENVIRONMENTNAME}"

# Check Beanstalk events for ERROR's.
# Break the Jenkins build so that we know there were some deploy errors.
ERROR_EVENTS=$(aws elasticbeanstalk describe-events --environment-name "${ENVIRONMENTNAME}" |
  jq -r "[.Events[] | select((.Severity == \"ERROR\") and .EventDate > \"${AWS_TIMESTAMP_NOW}\")] | length")
if [[ "${ERROR_EVENTS}" > "1" ]]; then
  echo "Beanstalk environment is in Ready state, but deployment has errors!"
  exit 1
fi

# Display all the events happened during this deployment
aws elasticbeanstalk describe-events --environment-name "${ENVIRONMENTNAME}" |
  jq -r ".Events[] | select(.EventDate > \"${AWS_TIMESTAMP_NOW}\") | \"\(.Severity)\t\(.Message)\""

# Invalidate directories that contains "assets"
if [[ -n $CLOUDFRONT_ID ]]; then
  aws cloudfront create-invalidation \
      --distribution-id "${CLOUDFRONT_ID}" \
      --paths "/wp-content*" "/wp-includes*" "/wp-admin*"
fi

# New Relic deployment notification
# Only in production and if the variable "NEW_RELIC_API_KEY" is actually set
if ${PRODUCTION} && [[ -n $NEW_RELIC_API_KEY ]]; then
  curl -H "x-api-key:${NEW_RELIC_API_KEY}" \
    -d "deployment[application_id]=${NEW_RELIC_APP_ID}" \
    -d "deployment[description]=${VERSIONLABEL}" \
    -d "deployment[revision]=${GIT_COMMIT}" \
    -d "deployment[user]=${BUILD_USER}" \
    https://api.newrelic.com/deployments.xml
fi

ENDJOBTIME=$(date +%s)
EXECUTETIME=$((ENDJOBTIME - STARTJOBTIME))
echo "STATUSREPORT: ${ENVIRONMENTNAME} - Ready :) Total execution time: ${EXECUTETIME} seconds."
